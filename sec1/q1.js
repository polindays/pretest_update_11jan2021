//q1_1.js

const fetch = require("node-fetch");

function thisIsSyncFunction() {
  let result = 0;
  return (async () => {
    return fetch("https://codequiz.azurewebsites.net/data")
      .then((res) => res.json())
      .then((response) => {
        result = response.data;
        console.log(result);
        return result;
      });
  })();
}

const number1 = thisIsSyncFunction();
const calculation = number1 * 10;
console.log(calculation);
