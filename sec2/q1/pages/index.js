import { useState, useMemo } from "react";

function isSquare(n) {
  return n > 0 && Math.sqrt(n) % 1 === 0;
}

function fibonacci(num) {
  if (isSquare(5 * (num * num) - 4) || isSquare(5 * (num * num) + 4)) {
    return "true";
  } else {
    return "false";
  }
}

export default function Home() {
  const theme = {
    col1: {
      display: "table-cell",
      minWidth: 200,
      border: "1px solid black",
      height: "100vh",
    },
    col2: {
      display: "table-cell",
      border: "1px solid black",
      height: "100vh",
      minWidth: 100,
      width: "calc(100vw - 500px)",
    },
    col3: {
      display: "table-cell",
      border: "1px solid black",
      minWidth: 300,
      height: "100vh",
    },
  };

  const [val, setVal] = useState("");
  const [calVal, setCalVal] = useState(1);
  const [mode, setMode] = useState("isPrime");
  const [ans, setAns] = useState("");

  useMemo(() => {
    if (mode === "isPrime") {
      var isPrime = (calVal > 1).toString();
      for (let i = 2; i < calVal; i++)
        if (calVal % i === 0) {
          isPrime = "false";
          break;
        }
      setAns(isPrime);
    } else {
      setAns(fibonacci(calVal));
    }
  }, [calVal, mode]);

  function handleInput(val) {
    if (val !== "" && val !== "-") {
      const valInt = parseFloat(val);
      const cal = valInt < 0 ? 1 : Math.round(valInt);
      setCalVal(cal);
    }
    setVal(val);
  }

  return (
    <div style={{ overflow: "auto" }}>
      <div style={theme.col1}>
        <input
          type="text"
          value={val}
          onChange={(e) => handleInput(e.target.value)}
        />
      </div>
      <div style={theme.col2}>
        <select value={mode} onChange={(e) => setMode(e.target.value)}>
          <option value="isPrime">isPrime</option>
          <option value="isFibonacci">isFibonacci</option>
        </select>
      </div>
      <div style={theme.col3}>
        {mode}({calVal}) = {ans}
      </div>
    </div>
  );
}
