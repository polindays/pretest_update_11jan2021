const axios = require("axios");

const apiUrl = "https://codequiz.azurewebsites.net";

const code = process.argv[2].trim();

const replace = "<tr>/s|<td>" + code + ".*?</td></tr>";
const replaceNav = "<td>[0-9.]*?</td>";

const re = new RegExp(replace, "g");
const reNav = new RegExp(replaceNav, "g");

(async () => {
  var code2NAV = {};
  const res = await (
    await axios.get(apiUrl, {
      headers: {
        Cookie: "hasCookie=true",
      },
    })
  ).data;
  tableExtracted = res.match(re);
  if (!tableExtracted) return;
  const tdNav = tableExtracted[0].match(reNav);
  if (!tdNav) return;
  console.log(tdNav[0].substring(4, tdNav[0].length - 5));
})();
