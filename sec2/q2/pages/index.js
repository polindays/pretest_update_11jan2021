import { useState, useMemo } from "react";

const apiUrl = "https://api.publicapis.org/categories";

export default function Home() {
  const [categories, setCategories] = useState([]);
  const [filter, setFilter] = useState("");

  useMemo(async () => {
    const res = await (await fetch(apiUrl)).json();
    setCategories(res);
  });

  useMemo(() => {}, [filter]);

  return (
    <>
      <input
        type="text"
        value={filter}
        onChange={(e) => setFilter(e.target.value)}
      />
      <br />
      <table>
        <tbody>
          {categories
            .filter((e) => e.includes(filter))
            .map((e) => (
              <tr key={e}>
                <td>{e}</td>
              </tr>
            ))}
        </tbody>
      </table>
    </>
  );
}
